# Análisis de Sentimientos utilizando Comentarios de Facebook

## Cargar el archivo
#Se extrajeron 1,569 comentarios de septiembre y octubre año 2020 en los que se menciona a CitiBanamex

#### Instalar Librerías
install.packages("syuzhet")
install.packages("RColorBrewer")
install.packages("wordcloud")
install.packages("tm")

#### Activar las Librerías
library(syuzhet)
library(RColorBrewer)
library(wordcloud)
library(tm)

#### Cargar los Datos
datos<-read.csv(file.choose(), stringsAsFactors = F, encoding="UTF-8")
head(datos)

#### utilizamos la función 'get_nrc_sentiment' para realizar el conteo de palabras correspondientes a cada una de las emociones de Plutchick
matsent <- get_nrc_sentiment(datos$com, lang="spanish")

barplot(colSums(prop.table(matsent[, 1:8])),
        col = brewer.pal(n = 8, name = "Set3"),
        main = "Distribución Porcentual de Frecuencias de Emociones")

barplot(colSums(prop.table(matsent[, 9:10])),
        col = brewer.pal(n = 8, name = "Set3"),
        main = "Distribución Porcentual de Términos Positivos y Negativos")

#### Calculamos un índice de Positividad del Comentario
matsent$Prop <- matsent$positive/(matsent$positive+matsent$negative)
head(matsent)

#### Unimos los dos DataFrames para tener la información completa
datos <- data.frame(datos,matsent)
head(datos)

#### Exportamos para generar reportes y otros análisis
write.csv(datos, "Comentarios.csv")

## Visualización de Wordcloud
#### Convertimos el tipo de Objeto de la Columna de Comentario

docs <- Corpus(VectorSource(datos[4]))

#### Removemos Stopwords o términos no relevantes
docs <- tm_map(docs, removeWords, stopwords("spanish"))
docs <- tm_map(docs, removeWords, c("citibanamex"))

#### Calculamos las frecuencias de cada Término
dtm <- DocumentTermMatrix(docs)
freq <- sort(colSums(as.matrix(dtm)), decreasing=TRUE)

#### Dibujamos el WordCloud
set.seed(2000)
wordcloud(names(freq), freq, min.freq=30, scale=c(5, .05), colors=brewer.pal(6, "Dark2"))
